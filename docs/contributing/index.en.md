# Guide to contribute to the project
Thank you very much for your interest in participating!

All content is maintained by volunteers who contribute their time and effort. Before we start: we ask you to be ** kind and respectful ** when participating.

We are currently looking for:

- **Improve the code** of the game engine (make it compatible with Python 3 and style fixes)

- Create a **platform (possibly in Django)** to organize the participants and show the development of the competition

Feel free to take any pending problems or you can directly write to our [group in Telegram](https://t.me/hispawars).