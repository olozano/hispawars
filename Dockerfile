FROM python:3.9

WORKDIR /hispawars/hispawars

COPY . /hispawars

RUN apt-get upgrade -y
RUN pip install -r ../requirements.txt
RUN chmod u+x play.sh

ENTRYPOINT ["./play.sh"]
