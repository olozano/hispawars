#!/usr/bin/env sh
DETECTED_DIR=`dirname "$0"`
export PYTHONPATH=$DETECTED_DIR/game
./game/playgame.py -e --capture_errors --end_wait=0.25 --verbose --log_dir ../logs --turns 300 --map_file game/sample.map "python base_bot/sample_bot.py" "python base_bot/sample_bot.py"
